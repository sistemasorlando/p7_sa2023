use db;

create table estudiantes(
	id int not null AUTO_INCREMENT,
	nombre varchar(50) not null,
    primary key (id)
);

insert into estudiantes(nombre) values ("JUAN PEREZ");
insert into estudiantes(nombre) values ("PEDRO PABLO");